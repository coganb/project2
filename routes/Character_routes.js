var express = require('express');
var router = express.Router();
var Character_dal = require('../model/Character_dal');
var Game_dal = require('../model/Game_dal');

router.get('/all', function(req, res) {
    Character_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Character/CharacterViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.character_id == null) {
        res.send('character_id is null');
    }
    else {
        Character_dal.getById(req.query.character_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Character/CharacterViewById', {'result': result});
           }
        });
    }
});

router.get('/add', function(req, res){
    Game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Character/CharacterAdd', {'Game': result});
        }
    });
});

router.get('/insert', function(req, res){

    if(req.query.Character_name == "") {
        res.send('Character Name must be provided.');
    }
    else if(req.query.is_playable == "")
    {
        res.send('Character is_playable must be provided.');
    }
    else if(req.query.is_killable == "")
    {
        res.send('Character is_killable must be provided.');
    }
    else {
        Character_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/Character/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.character_id == null) {
        res.send('A Character id is required');
    }
    else {
        Character_dal.edit(req.query.character_id, function(err, result){
            res.render('Character/CharacterUpdate', {Character: result[0][0]/*, address: result[1]*/});
        });
    }

});


router.get('/update', function(req, res) {
    Character_dal.update(req.query, function(err, result){
       res.redirect(302, '/Character/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.character_id == null) {
        res.send('character_id is null');
    }
    else {
         Character_dal.delete(req.query.character_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/Character/all');
             }
         });
    }
});

module.exports = router;
