var express = require('express');
var router = express.Router();
var Platform_dal = require('../model/Platform_dal');
var Player_dal = require('../model/Player_dal');
var Player_Platform_dal = require('../model/Player_Platform_dal');

router.get('/all', function(req, res) {
    Player_Platform_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Player_Platform/UsernameViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        Hours_dal.getById(req.query.player_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Hours/HoursViewById', {'result': result});
           }
        });
    }
});

router.get('/add', function(req, res){
    Platform_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            Player_dal.getAll(function(err,secondresult){
                if (err) {
                    res.send(err);
                } else {
                    res.render('Player_Platform/UsernameAdd', {'Platform': result, 'Player':secondresult});
                }
            });

        }
    });
});

router.get('/insert', function(req, res){
    if(req.query.platform_id == null) {
        res.send('A platform must be provided.');
    }
    else if(req.query.player_id == null)
    {
        res.send('player must be provided.');
    }
    else if(req.query.username == "")
    {
        res.send('a username must be provided.');
    }
    else {
        Player_Platform_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/Player_Platform/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.Player_id == null) {
        res.send('A players id is required');
    }
    else if(req.query.platform_id == null) {
        res.send('A Platforms id is required')
    }
    else if(req.query.username == null) {
        res.send('A username is required')
    }
    else {
        Player_Platform_dal.edit(req.query.Player_id, req.query.platform_id, req.query.username, function(err, result){
            res.render('Player_Platform/UsernameUpdate', {Player_Platform: result[0][0]});
        });
    }

});



router.get('/update', function(req, res) {
    Game_dal.update(req.query, function(err, result){
       res.redirect(302, '/Game/all');
    });
});

router.get('/delete', function(req, res){

    if(req.query.Platform_id == null){
        res.send('the Platform id is null')
    }
    else if(req.query.username == null){
        res.send('the username is null')
    }
    else {
         Player_Platform_dal.delete(req.query.Platform_id, req.query.username, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/Player_Platform/all');
             }
         });
    }
});

module.exports = router;
