var express = require('express');
var router = express.Router();
var Game_dal = require('../model/Game_dal');
var Player_dal = require('../model/Player_dal');
var Hours_dal = require('../model/Hours_dal');

router.get('/all', function(req, res) {
    Hours_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Hours/HoursViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        Hours_dal.getById(req.query.player_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Hours/HoursViewById', {'result': result});
           }
        });
    }
});


router.get('/add', function(req, res){
    Game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            Player_dal.getAll(function(err,secondresult){
                if (err) {
                    res.send(err);
                } else {
                    res.render('Hours/HoursAdd', {'Game': result, 'Player':secondresult});
                }
            });

        }
    });
});

router.get('/insert', function(req, res){
    if(req.query.game_id == null) {
        res.send('Game Name must be provided.');
    }
    else if(req.query.player_id == null)
    {
        res.send('player must be provided.');
    }
    else if(req.query.timeplayed == null)
    {
        res.send('time must be provided.');
    }
    else if(req.query.hours_played == null)
    {
        res.send('hours played must be provided.');
    }
    else {
        Hours_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/Hours/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.game_id == null) {
        res.send('A Game id is required');
    }
    else {
        Game_dal.edit(req.query.game_id, function(err, result){
            res.render('Game/GameUpdate', {Game: result[0][0]});
        });
    }

});





router.get('/update', function(req, res) {
    Game_dal.update(req.query, function(err, result){
       res.redirect(302, '/Game/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
         Game_dal.delete(req.query.game_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/Game/all');
             }
         });
    }
});

module.exports = router;
