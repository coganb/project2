var express = require('express');
var router = express.Router();
var DLC_dal = require('../model/DLC_dal');
var Game_dal = require('../model/Game_dal');


router.get('/all', function(req, res) {
    DLC_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('DLC/DLCViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.DLC_id == null) {
        res.send('DLC_id is null');
    }
    else {
        DLC_dal.getById(req.query.DLC_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('DLC/DLCViewById', {'result': result});
           }
        });
    }
});


router.get('/add', function(req, res){
    Game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('DLC/DLCAdd', {'Game': result});
        }
    });
});

router.get('/insert', function(req, res){
    if(req.query.DLC_name == "") {
        res.send('DLC Name must be provided.');
    }
    else if(req.query.DLC_price == "")
    {
        res.send('DLC DLC_price must be provided.');
    }
    else if(req.query.Content_type == "")
    {
        res.send('DLC Content_type must be provided.');
    }
    else {
        DLC_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/DLC/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.DLC_id == null) {
        res.send('A DLC id is required');
    }
    else {
        DLC_dal.edit(req.query.DLC_id, function(err, result){
            res.render('DLC/DLCUpdate', {DLC: result[0][0]/*, address: result[1]*/});
        });
    }

});


router.get('/update', function(req, res) {
    DLC_dal.update(req.query, function(err, result){
       res.redirect(302, '/DLC/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.DLC_id == null) {
        res.send('DLC_id is null');
    }
    else {
         DLC_dal.delete(req.query.DLC_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/DLC/all');
             }
         });
    }
});

module.exports = router;
