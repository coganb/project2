var express = require('express');
var router = express.Router();
var Platform_dal = require('../model/Platform_dal');

router.get('/all', function(req, res) {
    Platform_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Platform/PlatformViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.Platform_id == null) {
        res.send('Platform_id is null');
    }
    else {
        Platform_dal.getById(req.query.Platform_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Platform/PlatformViewById', {'result': result});
           }
        });
    }
});

router.get('/add', function(req, res){
    Platform_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Platform/PlatformAdd', {'Platform_type': result});
        }
    });
});

router.get('/insert', function(req, res){

    if(req.query.Platform_type == "") {
        res.send('Platform Name must be provided.');
    }
    else if(req.query.price == "")
    {
        res.send('Platform price must be provided.');
    }
    else if(req.query.operating_system == "")
    {
        res.send('Platform operating_system must be provided.');
    }
    else {
        Platform_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/Platform/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.Platform_id == null) {
        res.send('A Platform id is required');
    }
    else {
        Platform_dal.edit(req.query.Platform_id, function(err, result){
            res.render('Platform/PlatformUpdate', {Platform: result[0][0]});
        });
    }

});



router.get('/update', function(req, res) {
    Platform_dal.update(req.query, function(err, result){
       res.redirect(302, '/Platform/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.Platform_id == null) {
        res.send('Platform_id is null');
    }
    else {
         Platform_dal.delete(req.query.Platform_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/Platform/all');
             }
         });
    }
});

module.exports = router;
