var express = require('express');
var router = express.Router();
var Player_dal = require('../model/Player_dal');


router.get('/all', function(req, res) {
    Player_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Player/PlayerViewAll', { 'result':result });
        }
    });

});


router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        Player_dal.getById(req.query.player_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Player/PlayerViewById', {'result': result});
           }
        });
    }
});


router.get('/add', function(req, res){
    Player_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Player/PlayerAdd', {'email': result});
        }
    });
});


router.get('/insert', function(req, res){
    if(req.query.email == "") {
        res.send('Player Name must be provided.');
    }
    else if(req.query.preffered_genre == "")
    {
        res.send('Player preffered_genre must be provided.');
    }
    else {
        Player_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/Player/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.player_id == null) {
        res.send('A Player id is required');
    }
    else {
        Player_dal.edit(req.query.player_id, function(err, result){
            res.render('Player/PlayerUpdate', {Player: result[0][0]});
        });
    }

});



router.get('/update', function(req, res) {
    Player_dal.update(req.query, function(err, result){
       res.redirect(302, '/Player/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
         Player_dal.delete(req.query.player_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/Player/all');
             }
         });
    }
});

module.exports = router;
