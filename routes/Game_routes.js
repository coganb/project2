var express = require('express');
var router = express.Router();
var Game_dal = require('../model/Game_dal');

router.get('/all', function(req, res) {
    Game_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Game/GameViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
        Game_dal.getById(req.query.game_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Game/GameViewById', {'result': result});
           }
        });
    }
});

router.get('/add', function(req, res){
    Game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Game/GameAdd', {'g_name': result});
        }
    });
});

router.get('/insert', function(req, res){

    if(req.query.g_name == "") {
        res.send('Game Name must be provided.');
    }
    else if(req.query.price == "")
    {
        res.send('Game price must be provided.');
    }
    else if(req.query.genre == "")
    {
        res.send('Game Genre must be provided.');
    }
    else {
        Game_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/Game/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.game_id == null) {
        res.send('A Game id is required');
    }
    else {
        Game_dal.edit(req.query.game_id, function(err, result){
            res.render('Game/GameUpdate', {Game: result[0][0]});
        });
    }

});


router.get('/update', function(req, res) {
    Game_dal.update(req.query, function(err, result){
       res.redirect(302, '/Game/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
         Game_dal.delete(req.query.game_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/Game/all');
             }
         });
    }
});

module.exports = router;
