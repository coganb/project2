var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM Player_Platform';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(player_id, callback) {
    var query = 'SELECT g.g_name, SUM(pg.hours_played) AS hours ' +
    'FROM Player_Game pg '+
    'LEFT JOIN Game g ON g.game_id = pg.game_id '+
    'WHERE pg.player_id = ?'+
    'GROUP BY g.g_name' +
    ';';

    var queryData = [player_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO Player_Platform(Platform_id, player_id, username) VALUES (?)';

    var queryData = [params.platform_id, params.player_id, params.username];

    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });
};

exports.delete = function(Platform_id, username, callback) {
    var query = 'DELETE FROM Player_Platform WHERE Platform_id=? AND username=?';
    var queryData = [Platform_id, username];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE Player_Platform SET username=? WHERE player_id = ? AND Platform_id=? AND username=?';
    var queryData = [params.newusername, params.Player_id, params.Platform_id, params.username];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};


exports.edit = function(game_id, callback) {
    var query = 'CALL Game_getinfo(?)';
    var queryData = [game_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};