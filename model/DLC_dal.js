var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);



exports.getAll = function(callback) {
    var query = "SELECT dlc.*, g.g_name from DLC dlc" +
    " LEFT JOIN Game g ON g.Game_id = dlc.Game_id;";

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(DLC_id, callback) {
    var query = "SELECT dlc.*, g.g_name from DLC dlc" +
    " LEFT JOIN Game g ON g.Game_id = dlc.Game_id WHERE DLC_id = ?";

    var queryData = [DLC_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {


    var query = 'INSERT INTO DLC (game_id, DLC_name, DLC_price, Content_type) VALUES (?)';


    var queryData = [params.game_id, params.DLC_name, params.DLC_price, params.Content_type];




    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });

};

exports.delete = function(DLC_id, callback) {
    var query = 'DELETE FROM DLC WHERE DLC_id = ?';
    var queryData = [DLC_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE DLC SET DLC_price=? WHERE DLC_id = ?';
    var queryData = [params.DLC_price, params.DLC_id];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};



exports.edit = function(DLC_id, callback) {
    var query = 'CALL DLC_getinfo(?)';
    var queryData = [DLC_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};