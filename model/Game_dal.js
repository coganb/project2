var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM Game_Price;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(game_id, callback) {
    var query = 'SELECT * FROM Game_Price WHERE game_id = ?';

    var queryData = [game_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {


    var query = 'INSERT INTO Game (g_name, price, genre) VALUES (?)';

    var queryData = [params.g_name, params.price, params.genre];

    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });

};

exports.delete = function(game_id, callback) {
    var query = 'DELETE FROM Game WHERE game_id = ?';
    var queryData = [game_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE Game SET g_name=?, price = ?, genre = ? WHERE game_id = ?';
    var queryData = [params.g_name, params.price, params.genre, params.game_id,];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};



exports.edit = function(game_id, callback) {
    var query = 'CALL Game_getinfo(?)';
    var queryData = [game_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};