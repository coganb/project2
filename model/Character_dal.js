var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);



exports.getAll = function(callback) {
    var query = "SELECT characters.*, g.g_name from Characters characters" +
    " LEFT JOIN Game g ON g.game_id = characters.game_id;";

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};




exports.insert = function(params, callback) {

    var query = 'INSERT INTO Characters (game_id, c_name, is_playable, is_killable) VALUES (?)';


    var queryData = [params.game_id, params.c_name, params.is_playable, params.is_killable];




    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });

};

exports.delete = function(character_id, callback) {
    var query = 'DELETE FROM Characters WHERE character_id = ?';
    var queryData = [character_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE Characters SET Characters_price=? WHERE Characters_id = ?';
    var queryData = [params.Characters_price, params.Characters_id];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};



exports.edit = function(Characters_id, callback) {
    var query = 'CALL Characters_getinfo(?)';
    var queryData = [Characters_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};