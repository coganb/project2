var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);



exports.getAll = function(callback) {
    var query = 'SELECT * FROM Platform;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(Platform_id, callback) {
    var query = 'SELECT * FROM Platform WHERE Platform_id = ?';

    var queryData = [Platform_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Platform (Platform_type, price, operating_system) VALUES (?)';

    var queryData = [params.Platform_type, params.price, params.operating_system];

    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });

};

exports.delete = function(Platform_id, callback) {
    var query = 'DELETE FROM Platform WHERE Platform_id = ?';
    var queryData = [Platform_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE Platform SET Platform_type=?, price = ?, operating_system = ? WHERE Platform_id = ?';
    var queryData = [params.Platform_type, params.price, params.operating_system, params.Platform_id,];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};


exports.edit = function(Platform_id, callback) {
    var query = 'CALL Platform_getinfo(?)';
    var queryData = [Platform_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};