var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = ' SELECT pg.player_id, p.email, SUM(pg.hours_played) AS hours '+
    'FROM Player_Game pg '+
    'LEFT JOIN Player p ON p.player_id = pg.player_id '+
    'GROUP BY p.email;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(player_id, callback) {
    var query = 'SELECT g.g_name, SUM(pg.hours_played) AS hours ' +
    'FROM Player_Game pg '+
    'LEFT JOIN Game g ON g.game_id = pg.game_id '+
    'WHERE pg.player_id = ?'+
    'GROUP BY g.g_name' +
    ';';

    var queryData = [player_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Player_Game(player_id, game_id, time_last_played, hours_played) VALUES (?)';

    var queryData = [params.player_id, params.game_id, params.timeplayed, params.hours_played];

    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });

};

exports.delete = function(game_id, callback) {
    var query = 'DELETE FROM Game WHERE game_id = ?';
    var queryData = [game_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE Game SET g_name=?, price = ?, genre = ? WHERE game_id = ?';
    var queryData = [params.g_name, params.price, params.genre, params.game_id,];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};



exports.edit = function(game_id, callback) {
    var query = 'CALL Game_getinfo(?)';
    var queryData = [game_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};