var mysql   = require('mysql');
var db  = require('./db_connection.js');


var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM Player;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(player_id, callback) {
    var query = 'SELECT p.*, pp.username FROM Player p '+
        'LEFT JOIN Player_Platform pp ON pp.player_id = p.player_id ' +
        'WHERE p.player_id = ?';

    var queryData = [player_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {


    var query = 'INSERT INTO Player (email, preffered_genre) VALUES (?)';

    var queryData = [params.email, params.preffered_genre];

    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });

};

exports.delete = function(player_id, callback) {
    var query = 'DELETE FROM Player WHERE player_id = ?';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE Player SET email=?, preffered_genre = ? WHERE player_id = ?';
    var queryData = [params.email, params.preffered_genre, params.player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};



exports.edit = function(player_id, callback) {
    var query = 'CALL Player_getinfo(?)';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};